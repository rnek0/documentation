# Ver, compartir y descargar un vídeo

## Ver un vídeo

![interface lecteur vidéo](./assets/es-video-player-watch.png)

El reproductor de vídeo está compuesto por&nbsp;:

  1. <i data-feather="play"></i> y <i data-feather="pause"></i> , un botón para reproducir o pausar el vídeo
  1. <i data-feather="skip-forward"></i> , un botón para ir al vídeo siguiente
  1. un indicador de tiempo (transcurrido / total)
  1. estadísticas P2P (si está habilitado; de lo contrario, verá **HTTP** en su lugar)
  1. <i data-feather="volume-2"></i> o <i data-feather="volume-x"></i> para silenciar o reactivar el sonido del video
  1. controlador de volumen: puede utilizar las flechas de su teclado para subir / bajar el volumen (después de haber hecho clic previamente en el icono)
  1. <i data-feather="settings"></i> un botón para los parámetros: cambiar la calidad, la velocidad, (des) activar los subtítulos, etc ...
  1. modo cine para ampliar la pantalla
  1. modo de pantalla completa

## Compartir un vídeo

Para compartir un vídeo, primero debes ir a la página del vídeo que quieres compartir. Si eres el editor, puedes listar todos tus vídeos a través de la opción _Mis Vídeos_ en el menu de la izquierda. Una vez en la pagina del vidéo, solo basta con pinchar en el boton <i data-feather="share-2"></i> **COMPARTIR**&nbsp;:

?> los pequeños iconos al final de la línea te permiten copiar la URL pinchando en ellos.

## URL

Dirección del video, por ejemplo&nbsp;: `https://framatube.org/videos/watch/9c9de5e8-0a1e-484a-b099-e80766180a6d`. Esta dirección puede ser enviada a su contacto como usted desee, asi él podra con ella acceder directamente al vídeo.

![Modale partage url](./assets/es_share_url.png)

![modale partage url +](./assets/es_share_url_more.png)

## QR code

Se puede compartir un vídeo por médio de un QR-code&nbsp;:

![modale partage par qr code](./assets/es_share_qr_code.png)

![modale partage qr code +](./assets/es_share_qr_code_more.png)

## Incrustar

Un código de incrustación le permite insertar un reproductor de vídeo en su pagina web. Cuando un usuario comparte un enlace de PeerTube, la aplicación expone la información utilizando el formato OEmbed que permite a los sitios web externos incrustar automáticamente el vídeo (es decir, inyectar el reproductor de PeerTube). Algunas plataformas hacen esto con cualquier enlace externo (Twitter, Mastodon...) pero otras plataformas pueden requerir una configuración adicional.

![modale partage integration](./assets/es_share_integration.png)

![modale partage integration +](./assets/es_share_integration_more.png)

## Más personalización

Para cada opción, puede :

  * establecer una hora de inicio haciendo clic en **Empezar en** y cambiar la marca de tiempo pinchando en ella.
  * si el vídeo dispone de subtítulos, elija mostrar uno por defecto haciendo clic en **Selección automática de subtítulos**.

También puede personalizar un poco más haciendo clic en el botón **Más personalización**:

  * **Empezar en**: elige el momento en el que quieres que empiece el vídeo ;
  * **Detener en**: elige el momento en el que quieres parar el vídeo ;
  * **Reproducción automática**: haz clic en este botón si quieres que el vídeo se inicie automáticamente ;
  * **Silencio**: haga clic en este botón si desea que el vídeo se reproduzca sin sonido (el usuario puede cancelar esta configuración durante la reproducción) ;
  * **Bucle**: haga clic si desea que el vídeo se repita;
  * **Mostrar el título del vídeo** (sólo para **Incrustación**): Desmarque si no quiere mostrar el título del vídeo ;
  * **Mostrar advertencia de privacidad** (sólo para **Incrustación**): Desmarque si no desea mostrar el mensaje de advertencia "Ver este vídeo puede revelar su dirección IP a otras personas";
  * **Mostrar controles del reproductor** (sólo para **Incrustación**): Desmarque si no quiere mostrar los botones de reproducción/pausa, etc. ;
  * **Mostrar enlace del botón PeerTube** (sólo para **Incrustación**): permite mostrar un enlace **PEERTUBE** en el reproductor para ver el vídeo en la instancia que lo aloja.

## Atajos de teclado

Puedes utilizar atajos para realizar algunas acciones. Para que aparezca una lista de estos atajos puedes hacer clic en tu avatar y luego **<i data-feather="command"></i>Atajos de teclado** (o **Atajos de teclado** directamente en la parte inferior del menú lateral izquierdo si no estás conectado⋅e).
También se puede utilizar `?`.

  * `?`	Mostrar / ocultar este menú de ayuda
  * `esc`	Ocultar este menú de ayuda
  * `/s`	Enfocar la barra de búsqueda
  * `b`	Conmutar el menú de la izquierda
  * `g o`	Ir a la página de descubrir videos
  * `g t`	Ir a la página de vídeos populares
  * `g r`	Ir a la página de vídeos recientes
  * `g l`	Ir a la página de vídeos locales
  * `g u`	Ir a la página para subir vídeos
  * `f`	Entrar/salir del modo de pantalla completa (requiere el foco en el reproductor de vídeo)
  * `space`	Reproducir/Pausar vídeo (requiere el foco en el reproductor de vídeo)
  * `m`	Désactiver/Activer le son de la vidéo (requiere el foco en el reproductor de vídeo)
  * `0-9`	Saltar a un porcentaje del vídeo: 0 es 0% y 9 es 90%. (requiere el foco en el reproductor de vídeo)
  * `↑` Aumentar el volumen (requiere el foco en el reproductor de vídeo)
  * `↓`	Disminuir el volumen (requiere el foco en el reproductor de vídeo)
  * `→`	Avanzar el vídeo (requiere el foco en el reproductor de vídeo)
  * `←`	Rebobinar el vídeo (requiere el foco en el reproductor de vídeo)
  * `>`	Aumentar la velocidad de reproducción (requiere el foco en el reproductor de vídeo)
  * `<`	Disminuir la velocidad de reproducción (requiere el foco en el reproductor de vídeo)
  * `.`	Navegar por el vídeo fotograma a fotograma (requiere el foco en el reproductor de vídeo)
