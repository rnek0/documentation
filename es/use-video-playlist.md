# Listas de reproducción

Al igual que otras plataformas de vídeo, una lista de reproducción en PeerTube es una colección ordenada de vídeos que se pueden compartir o mantener privados. Puedes añadir libremente tus propios vídeos o los de otras personas a una lista de reproducción.

Ya tienes en Peertube una lista de reproduccion llamada : "Ver más tarde " !

## Crear una lista de reproducción

Se pueden crear nuevas lista de reproducción pasando por  `Mi biblioteca` > `Listas de reproducción` , o sino directamente por el menu de la izquierda. A continuación, haga clic en el boton "Crear lista de reproducción".

![Lista de lista de reproducción de un usuario](./assets/es-video-playlist-list.png)

Tenga en cuenta que una lista de reproducción no puede estar asociada a ningún canal de vídeo, o a cualquier canal de su elección. Sin embargo, al crear una lista de reproducción pública, ésta debe pertenecer a un canal.

Puedes elegir una miniatura para la lista de reproducción, así como un título y una descripción. Será lo primero que vean los usuarios cuando hagan una búsqueda.


![Formulario para crear una lista de reproducción](./assets/es-video-playlist-creation.png)

## Añadir vidéos a las listas de reproducción

Las acciones rápidas que aparecen en las miniaturas al pasar el ratón por encima y pinchando en el boton de tres puntos, te permiten añadir rápidamente vídeos a tus listas de reproducción.

![Acciones rápidas para Añadir vidéos a une listas de reproducción](./assets/es-video-playlist-quick-action.png)

## Poner listas de reproducción en orden

Las listas de reproducción están ordenadas para que los espectadores puedan ver los vídeos de forma secuencial. Es usted quien debe ordenar los vídeos en su lista de reproducción una vez que los haya recopilado. Haga clic en el botón "Editar" de la lista de reproducción para acceder a una lista de órdenes de arrastre.

![Poner listas de reproducción en orden](./assets/es-video-playlist-ordering.png)

## Visualizar listas de reproducción

La visualización de una lista de reproducción activa un modo especial del reproductor de vídeo: los vídeos de la lista de reproducción actual se enumeran en un panel situado en la parte derecha del reproductor, para que pueda navegar rápidamente por ellos y ver los próximos vídeos.

![Ver una lista de reproducción](./assets/es-video-playlist-watching.png)
