# Descargar un vídeo

Puedes descargar videos directamente desde la interfaz web de PeerTube. Para ello, debes hacer clic en el botón **[...]** situado debajo del vídeo y despues haces clic en el botón <i data-feather = "download"> </i> **Descargar** : 

![Ventana modal presentando las opciones de descarga.](./assets/es-video-download.png)

Aparecerá un cuadro con las opciones por defecto: Descarga directa del vídeo con la mejor calidad. Sólo tienes que hacer clic en el botón **Descargar** para tener el vídeo en tu ordenador.

Al hacer clic en <i data-feather = "chevron-down"> </i> **Avanzado**, obtendrás información detallada sobre el vídeo, su formato, flujo de video y audio:

![modale avancée des options de téléchargement](./assets/es-use-download-modal-advanced.gif)

También tienes la posibilidad de descargar el vídeo por *torrent*.

?> **Descarga directa**&nbsp;:  el navegador web descarga el vídeo desde el servidor de vídeo original <br />
**Torrente (fichero .torrent)**&nbsp;: necesitas un cliente compatible con WebTorrent para descargar el vídeo, no sólo del servidor de origen sino también de otros pares que estén descargando el vídeo.

**Consejo** : Según la instancia, puedes descargar el vídeo en diferentes formatos. Sin embargo, asegúrate de haber obtenido primero una licencia compatible con el uso que has previsto para el vídeo.
