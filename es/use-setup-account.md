# Configurar su cuenta

## Crear una cuenta en PeerTube

Para poder subir un vídeo, debe tener una cuenta en una _instancia_ (un servidor en el cual esta instalado el Software de PeerTube). El proyecto PeerTube mantiene una lista de
instancias públicas en esta web : [instances.joinpeertube.org](https://instances.joinpeertube.org/instances) (notese que los administradores de instancias deben añadir ellos mismos su instancia a esta lista manualmente, esta no se genera automaticamente).

Una vez que haya encontrado la instancia que le conviene, dele al boton "Crear una cuenta" y indique el **nombre que se debe mostrar**,  su **nombre de identificación** (identificador), una **dirección de correo** y una **contraseña** :

![Pagina después de haber pinchado en "Crear una cuenta"](./assets/es-profile-registration-01.png)

Después de haber completado la primera etapa, pase a la segunda después de haber pinchado en el botón "Siguiente", indique el **nombre de visualización de su canal** y el **nombre de su canal** (identificador de su canal) :

![Pagina después de haber pinchado en "siguiente"](./assets/es-profile-registration-02.png)

Después de haber completado la segunda étapa, una confirmación de la creación de la cuenta se muestra después de hacer clic en el botón "Crear mi cuenta" :

![Pagina después del clic en "Crear una cuenta" de la segunda étapa](./assets/es-profile-registration-03.png)

Después de hacer clic en el enlace para verificar su cuenta y completar su registro :

![Pagina después del clic en el enlace de verificacion del correo](./assets/es-profile-registration-04.png)


## Conéctando a tu instancia de PeerTube

Para conectarse, debe dirigirse a la dirección de la instancia concreta en la que se ha registrado. Las instancias comparten los vídeos de los demás, pero el índice de las cuentas de cada instancia no está federado. Haga clic en el botón "Iniciar sesión" en la esquina superior izquierda, y luego introduzca su nombre de usuario o dirección de correo electrónico y contraseña. Tenga en cuenta que el nombre de usuario y la contraseña distinguen las mayúsculas y minúsculas (PeerTube distingue entre una "a" y una "A")

Una vez conectado, su identificador y su nombre apareceran bajo el nombre de la instancia.

## Actualizar su perfil

Para actualizar su perfil, cambian su avatar, cambiar su contraseña, etc… Debe hacer clic en <i data-feather="user"></i> **Mi cuenta** bajo su nombre de perfil en la barra lateral. A continuación, tiene varias opciones :

*  Ajustes
*  Notificaciones
*  Aplicaciones
*  Moderación
  * Cuentas silenciadas
  * Servidores silenciados
  * Informes de abuso

![Vista de la biblioteca de un usuario](./assets/es-profile-library.png)

### Mis ajustes

La sección de ajustes está dividida en varias partes:

#### CONFIGURACIÓN DE PERFIL

En esta secion usted puede :

* Subir su avatar enviando, desde su ordinador, une imagen en formato .png, .jpeg o .jpg con un peso máximo de de 100 Ko (el peso puede variar en función de las instancias).
* ver su cuota de peso utilizado / máximo. Esta cuota está determinada por la instancia que aloja su cuenta, y puede variar desde unos pocos MB hasta miles de GB, o incluso ilimitada. En las instancias que crean diferentes definiciones de sus vídeos después de subirlos (tras un paso de transcodificación de vídeo), se tiene en cuenta el espacio en disco que ocupan todas las versiones del vídeo, no sólo la que ha subido.
* cambiar el pseudonimo, que es diferente de su nombre de usuario. Su nombre de usuario no puede ser cambiado una vez creado.
* añadir una descripción de usuario, que se mostrará en su perfil público. Suele ser lo primero que ven los visitantes y usuarios de PeerTube, y representa tu identidad, así que no lo descuides.

#### AJUSTES DE VÍDEO

* En algunos casos, puedes elegir cómo mostrar los vídeos con contenido sensible: mostrar, difuminar miniaturas o "ocultar".
* Mostrar sólo los vídeos en los siguientes idiomas/subtítulos en las páginas **Recientes**, **Tendencias**, **Vídeos locales**, **Más populares** y **Búsqueda**.
* Ayudar a compartir vídeos mediante P2P.
* Iniciar inmediatamente la reproducción de un vídeo al llegar a su página.
* Reproducir automáticamente el siguiente vídeo.

#### NOTIFICACIONES

Puede establecer qué notificaciones desea recibir por correo electrónico y/o a través de la interfaz de la instancia.

#### INTERFAZ

Puedes cambiar la apariencia de tu instancia con un tema preinstalado.

#### CONTRASEÑA

Para cambia su contraseña, debe escribirla dos veces y hacer clic en **_Cambiar contraseña_**.

#### CORREO ELECTRÓNICO

Para cambiar la dirección de correo electrónico asociada a su cuenta, deberá introducir su contraseña actual para validar el cambio.

#### ZONA DE PELIGRO

Para eliminar su cuenta.

!> **Una vez que su cuenta es eliminada, no hay vuelta atrás. Se le pedirá que confirme esta acción.**
