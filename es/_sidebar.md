- Utilizar PeerTube

 - [Configurar su perfil](/es/use-setup-account.md)
 - [Ver, compartir y descargar un vídeo](/es/use-share-video.md)
 - [Descargar un vídeo](/es/use-download-video.md)
 - [Canal de vídeo](/es/use-video-channels.md)
 - [Listas de reproducción](/es/use-video-playlist.md)
 - [Publicar un vídeo o un directo](/es/use-create-upload-video.md)
 - [Historique des vidéos](/fr/use-video-history.md)
 - [Recherche](/fr/use-search.md)
 - [Signaler un contenu](/fr/use-report.md)

- Administrer PeerTube

 - [Following other instances](admin-following-instances.md)
 - [Managing users](admin-managing-users.md)
 - [Deal with spammers](admin-deal-with-spammers.md)

- Installer PeerTube

  - [Any OS (recommended)](install-any-os.md)
  - [Docker](install-docker.md)
  - [Unofficial](install-unofficial.md)


- Maintain PeerTube (sysadmin)

 - [CLI tools](maintain-tools.md)
 - [Instance migration](maintain-migration.md)
 - [Cache](maintain-cache.md)
 - [Security](maintain-security.md)
 - [Configuration](maintain-configuration.md)

- Contribuer à PeerTube

 - [Getting started](contribute-getting-started.md)
 - [Architecture](contribute-architecture.md)
 - [Code of conduct](contribute-code-of-conduct.md)

- PeerTube API

 - [Getting started with REST API](api-rest-getting-started.md)
 - [REST API reference](api-rest-reference.html)
 - [ActivityPub](api-activitypub.md)
