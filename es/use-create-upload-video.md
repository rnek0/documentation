# Publicar un vídeo o un directo

## Subir un vídeo

Para publicar un vídeo, se debe pinchar en el boton <i data-feather="upload-cloud"></i>**Publicar** arriba y a la derecha de la pagina. Después de ello tenemos 3 maneras de subir un fichero&nbsp;:

  1. seleccionando un fichero en tu dispositivo
  1. importando un video que ya este en linea con su URL
  1. importando un video en linea con su URI (torrente)

### Subir un archivo

Después de haber cliqueado en el boton <i data-feather="upload-cloud"></i>**Publicar** arriba y a la derecha de la pagina, llegara a la pestañia **Subir un archivo**. Debe usted entonces hacer la seleccion de&nbsp;:

  1. el **Canal** en el que sera guardado el video (se puede hacer o modificar después de haber subido el video)
  1. la **Privacidad** del video (se puede hacer o modificar después de haber subido el video)
  1. seleccione un archivo en su dispositivo con el formato adecuado pinchando en **Elige el archivo a subir**

Mientras sube el video se puede añadir [algunos detalles a su archivo de video](/es/use-create-upload-video?id=detalles-del-archivo)

### Importar con un URL

Si el administrador de su instancia a activado esta opcion, usted puede importar cualquier URL [soportada por youtube-dl](https://ytdl-org.github.io/youtube-dl/supportedsites.html) o un URL que apunte directamente a un fichero MP4. Para ello usted debe&nbsp;:

  1. pinchar en el boton <i data-feather="upload-cloud"></i>**Publier** en la parte superior derecha de la página
  1. pichar en la pestaña **Importar con URL**
  1. pagar el URL del archivo en el campo **URL**
  1. selecccionar el **Canal** en el que desea guardar el video (se puede modificar mas tarde)
  1. selecccionar la **Privacidad** del fichero (se puede modificar posteriormente)
  1. pinchar en el boton **Importar**.

!> Debe asegurarse de tener derechos de difusión sobre el contenido al que apunta, de lo contrario, les podría causar problemas legales a usted y a su instancia. 

Mientras sube el video se puede añadir [algunos detalles extra a su archivo de video](/es/use-create-upload-video?id=detalles-del-archivo)

### Importar con un torrente

Si el administrador de su instancia ha activado esta opcion, usted puede importar cualquier torrente que apunte directamente a un archivo MP4. Para ello es necesario&nbsp;:

  1. pinchar en el boton <i data-feather="upload-cloud"></i>**Publicar** en la parte superior derecha de la página
  1. pichar en la pestaña **Importar con torrent**
  1. selecccionar el archivo `.torrent` en su ordenador o pegar el URI magnet de un archivo
  1. selecccionar el **Canal** en el que désea guardar el archivo (se puede modificar posteriormente)
  1. selecccionar la **Privacidad** del fichero (se puede modificar posteriormente)
  1. pinchar en el boton **Importar**.

!> Debe asegurarse de tener derechos de difusión sobre el contenido al que apunta, de lo contrario, les podría causar problemas legales a usted y a su instancia.

Mientras sube el video se puede añadir [algunos detalles extra a su archivo de video](/es/use-create-upload-video?id=detalles-del-archivo)

## Transmitir un flujo en directo (con PeerTube >= v3)

Si el administrador de su instancia ha activado esta opcion, puedes crear una transmisión en vivo utilizando PeerTube y un software como (por ejemplo [OBS](https://obsproject.com/)). Para ello, debes&nbsp;:

  1. pinchar en el boton <i data-feather="upload-cloud"></i>**Publicar** en la parte superior derecha de la página
  1. pichar en la pestaña **Ir a en vivo**
  1. selecccionar el **Canal** en el que deseas emitir el directo
  1. selecccionar la **Privacidad** de esa transmision
  1. seleccionar entre Vida normal y En vivo permanente / recurrente **Ir a en vivo**.
  1. pinchar en el boton **Ir a en vivo**.

![interfaz web del directo en PeerTube](./assets/es-go-live-UI.png)

La duración máxima de su directo es 1h. Si su directo alcanza este límite, se cancelará automáticamente. 

En la pestaña **Configuracion del directo** usted puede&nbsp;:

  * ver **el URL de RTMP en vivo** a insertar en su aplicación de difusión de video
  * ver la **Clave de transmisión en vivo** asociada que debe anadir al programa de difusion. **Es una clave privada que permite al que la posée emitir un vídeo en directo&nbsp;: no se debe compartir&nbsp;!**
  * escoger entre **Publica automáticamente una reproducción cuando finaliza tu transmisión** o no, si el administrador de su instancia ha activado esta opcion&nbsp;: al final del directo, Peertube creara una redifusion en la misma URL que el directo
  * activar **Este es un directo permanente / recurrente** para retransmitir varias veces, las repeticiones no se pueden grabar. El URL no cambiara para sus expectadores, puede transmitir varias veces en un vivo permanente/recurrente, pero no puede guardar las tomas de sus directos. El URL no cambiara entre dos eventos y una déconnexion del flujo no parara el directo.

A partir de este momento ya estas listo para difundir el directo&nbsp;! En este ejemplo utilizaremos [el software OBS](https://obsproject.com/) para enviar el fujo de video hacia PeerTube pero puedes utilizar cualquier software que use [el protocolo RTMP](https://en.wikipedia.org/wiki/Real-Time_Messaging_Protocol).

  1. Abra OBS en su ordenador
  1. Pinche en **Ajustes** (o en **Archivo** en la barra superior puis sur **Ajustes**)
  1. Pinche en **Emisión** en el menu lateral izquierdo
  1. Seleccione **Servicio Personalizado** para el tipo de difusión
  1. Introduzca el **Url RTMP del directo** de PeerTube dans le champ **URL**
  1. Introduzca la **Clave de retransmission** de PeerTube en su campo **Clave de retransmission**
  1. **No pinche** en la casilla **Usar la autentificación** y valide dandole a **Aceptar**
  1. cuando este listo, pinche en el boton **Iniciar Transmisión** para emitir en su instancia PeerTube&nbsp;: habrá un pequeño retraso entre su pantalla y la emisión.

![obs paramètres image](./assets/es-live-obs-settings.png)

## Detalles del archivo

### Información básica

  * **Título** : el título del vídeo (por ejemplo, algo más atractivo que `mivideo.mp4` :wink:)
  * **Etiquetas** : Las etiquetas pueden utilizarse para sugerir recomendaciones pertinentes. Hay un máximo de cinco etiquetas. Presiona Enter para añadir una nueva etiqueta.
  * **Descripción** : el texto que desea que aparezca debajo del vídeo. Las descripciones de los vídeos están truncadas por defecto y requieren una acción manual para ampliarlas. Puede ver la representación debajo del área de edición. Soporta la sintaxis markdown.
  * **Canal** : el canal en el que se quiere poner el vídeo
  * **Categoría** : ¿cuál es la categoría del vídeo? (Noticias y política? ¿Arte? ¿Música? etc...)
  * **Licencia** :
    * Atribución
    * Atribución - Compartir Igual
    * Atribución - No Derivadas
    * Atribución - No Comercial
    * Atribución - No Comercial - Compartir Igual
    * Atribución - No Comercial - No Derivadas
    * Domminio publico
  * **Idioma** : ¿cuál es el Idioma principal del vídeo?
  * **Visibilidad** : Privado, Sin listar, Publico o Interno ([ver el significado](/es/use-create-upload-video?id=opciones-de-visibilidad-del-vídeo))
  * **Contiene contenido sensible** : Algunas instancias ocultan videos que contienen contenido explícito o para adultos de forma predeterminada.
  * **Publicar después de la transcodificación** : Si decides no esperar a la transcodificación antes de publicar el vídeo, quizás no se pueda reproducir hasta que finalice la transcodificación.

![Informaciones basicas - imagen](./assets/fr-user-upload-video-basic-info.png)

### Légendes

Cet onglet vous permet d'ajouter des sous-titres à votre vidéo. Pour en ajouter, vous devez&nbsp;:

  1. aller dans l'onglet **Légendes**
  1. cliquer sur le bouton <i data-feather="plus-circle"></i> **Ajouter un nouveau sous-titre**
  1. sélectionner une langue dans la liste
  1. cliquer sur **Choisir le fichier de sous-titre**
  1. sélectionner un fichier `.vtt` ou `.srt` sur votre ordinateur
  1. cliquer sur le bouton **Ajouter ce sous-titre**
  1. cliquer sur le bouton **<i data-feather="check-circle"></i> Mise à jour**

### Paramétrage avancé

Cet onglet vous permet&nbsp;:

  * de modifier l'image de prévisualisation de votre vidéo
  * d'ajouter un court texte pour dire aux gens comment ils peuvent vous soutenir (plateforme d'adhésion...) - supporte la syntaxe markdown
  * d'activer/désactiver les commentaires sur la vidéo
  * d'activer/désactiver le téléchargement de la vidéo

?> Pensez à cliquer sur le bouton **<i data-feather="check-circle"></i> Mise à jour** après vos modifications

### Opciones de visibilidad del vídeo

  * **Public** : votre vidéo est publique. Tout le monde peut la voir (en utilisant un moteur de recherche, un lien ou un embed) ;
  * **Interne** : seul un utilisateur authentifié ayant un compte sur votre instance peut voir votre vidéo. Les utilisateurs effectuant une recherche à partir d'une autre instance ou ayant le lien sans être authentifiés ne peuvent pas la voir ;
  * **Non listé** : seules les personnes ayant un lien privé peuvent voir cette vidéo ; la vidéo n'est pas visible sans son lien (ne peut être trouvée par une recherche) ;
  * **Privé** : seul vous pouvez voir la vidéo.
